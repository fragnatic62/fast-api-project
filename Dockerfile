# Use an official Python runtime as a parent image
FROM python:3.11.4-slim-bullseye

# Turns off buffering for easier container logging
ENV PYTHONDONTWRITEBYTECODE=1

# Update package list and install necessary packages
RUN apt-get update && apt-get install -y \
    python3 \
    python3-pip \
    nodejs \
    npm

# Display versions of Python, Node.js, and npm
RUN python3 --version && \
    node -v && \
    npm -v

# Set the working directory in the container
WORKDIR /app

# Copy the current directory contents into the container at /app
COPY . /app

# Install Node.js dependencies
RUN npm install

# Install any necessary dependencies
RUN pip install --no-cache-dir -r requirements.txt

# Define environment variable
ENV ORIGINS="http://127.0.0.1:8000/,http://localhost:80/,http://0.0.0.0:8000/"
ENV SECRET_KEY="NOT_CONFIGURED"

EXPOSE 80

# Run your application
CMD ["python", "-m", "uvicorn", "main:app", "--host", "0.0.0.0", "--port", "8000"]

#http://localhost/
#docker build -t drive-thru-app .
#docker run -l 0.0.0.0 -p 80:8000 drive-thru-app