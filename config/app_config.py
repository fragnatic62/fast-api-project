from pydantic_settings import BaseSettings, SettingsConfigDict


class AppConfig(BaseSettings):

    SECRET_KEY: str
    ORIGINS: str

    model_config = SettingsConfigDict(
        env_file='.env',
        env_file_encoding='utf-8'
    )


config = AppConfig()
