class StreamerService:
    _rtsp_root_url: str = 'static/videos/'
    _port: str = 'TestVideo.mp4'
    _rtsp_url: str = None

    def set_rstp_url(self, root_url: str, port: str):
        self._rtsp_root_url = root_url
        self._port = port
        self._rtsp_url = f'{self._rtsp_root_url}{self._port}'

    def get_rtsp_src_url(self) -> str:
        if not self._rtsp_url:
            self.set_rstp_url(self._rtsp_root_url, self._port)
        return self._rtsp_url
