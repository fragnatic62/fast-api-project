# import RPi.GPIO as GPIO


# class GPIOService:
#     _gpio: GPIO
#     _defaul_pin: int = 12

#     def set_gpio_svc(self, svc: GPIO = GPIO) -> None:
#         self._gpio = svc

#     def set_gpio_pin(self, pin: int) -> None:
#         """_summary_

#         Args:
#             pin (int): Provide the pin number you want to connect
#         """
#         self._defaul_pin = pin

#     def set_gpio_settings(self) -> None:
#         self._gpio.setup(self._defaul_pin, GPIO.IN, pull_up_down=GPIO.PUD_UP)

#     def read_gpio(self) -> int:
#         """
#         Reads the pin input
#         0 : indicate signal having a no contact or default
#         1: indicate signal which detected a contact

#         Returns:
#             int: return either 1 or 0
#         """
#         return GPIO.input(self._defaul_pin)
