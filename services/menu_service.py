from typing import List


class MenuImagesService:
    _root_url: str = 'static/images/400x400.png'

    def get_images(self) -> List[str]:
        images: List[str] = [f'{self._root_url}' for i in range(1, 17)]
        return images
