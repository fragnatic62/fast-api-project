# from services.gpio_service import GPIOService
from services.menu_service import MenuImagesService
from services.stream_service import StreamerService


class DriveThruServiceMixin:
    # gpio_svc: GPIOService
    menu_images_svc: MenuImagesService
    streamer_svc: StreamerService

    def initialize_services(self) -> None:
        """
        Initializes all the drivethru app services
        """
        # self.gpio_svc = GPIOService()
        self.menu_images_svc = MenuImagesService()
        self.streamer_svc = StreamerService()
