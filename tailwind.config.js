/** @type {import('tailwindcss').Config} */
module.exports = {
  content: [
    './**/*.html',     // HTML files
    './**/*.js',       // JavaScript files
    './**/*.jsx',      // JSX files (if you're using React)
    './**/*.ts',       // TypeScript files (if you're using TypeScript)
    './**/*.tsx',
    './**/*.css'
  ],
  theme: {
    extend: {},
  },
  plugins: [],
}

