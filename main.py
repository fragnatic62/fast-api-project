import asyncio
import datetime

from fastapi import FastAPI, Request
from fastapi.responses import HTMLResponse, StreamingResponse
from fastapi.staticfiles import StaticFiles
from fastapi.templating import Jinja2Templates
from fastapi.middleware.cors import CORSMiddleware

from config.app_config import config
from services.mixin import DriveThruServiceMixin


app = FastAPI()

app.add_middleware(
    CORSMiddleware,
    allow_origins=config.ORIGINS.split(','),
    allow_headers=['*'],
)


@app.middleware('http')
async def add_headers(request, call_next):
    response = await call_next(request)
    response.headers['Permissions-Policy'] = \
        'geolocation=(self), microphone=(self)'
    return response

# Initialize
drive_thru_svc_mixin = DriveThruServiceMixin()
drive_thru_svc_mixin.initialize_services()


# Mount the static directory to serve CSS and other static files
app.mount('/static', StaticFiles(directory='static'), name='static')

# Initialize Jinja2Templates to render HTML templates
templates = Jinja2Templates(directory='templates')


@app.get('/template/{template_name}', response_class=HTMLResponse)
async def get_template(request: Request, template_name: str):
    """
    Renders the template requested by the client
    """
    return templates.TemplateResponse(
        template_name,
        {
            'request': request,
            'stream_url': drive_thru_svc_mixin.streamer_svc.get_rtsp_src_url(),
            'images': drive_thru_svc_mixin.menu_images_svc.get_images(),
        }
    )


@app.get('/')
async def read_root(request: Request):
    """
    Renders the index.html
    """
    return templates.TemplateResponse(
        'index.html',
        {
            'request': request,
        }
    )


async def generate_time():
    # This function generates the current time in chunks asynchronously.
    while True:
        current_time = datetime.datetime.now().strftime("%S")
        yield current_time + "\n"
        await asyncio.sleep(1)  # Update every 1 second


@app.get('/gpio_stream')
async def stream():
    async def generate():
        async for chunk in generate_time():
            yield chunk

    response = StreamingResponse(
        media_type='text/plain',
        content=generate()
    )
    return response
